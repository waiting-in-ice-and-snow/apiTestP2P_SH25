import logging
import random
import unittest,requests,app

from parameterized import parameterized

from api.LoginAPI import LoginAPI
from utils import assert_utils, read_json_data


class TestLoginParams(unittest.TestCase):
    # 定义初始化方法
    def setUp(self) -> None:
        # 初始化API对象
        self.login_api = LoginAPI()
        # 初始化session
        self.session = requests.Session()

    # 定义清理方法
    def tearDown(self) -> None:
        # 关闭session
        self.session.close()

    # 随机小数获取图片验证码成功
    @parameterized.expand(read_json_data("imgCode.json"))
    def test01_get_img_code(self,desc,type,status_code):
        # 准备测试数据
        if type == "decimal":
            r = random.random()
        elif type == "interger":
            r = random.randint(100000,999999)
        elif type == "kong":
            r = ""
        elif type == "charactor":
            r = "".join(random.sample("abcdefghijklmn",8))
        else:
            print("参数输入错误")
            return
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(status_code,response.status_code)

    # # 参数正确时，获取短信验证码成功
    # def test05_get_sms_code_success(self):
    #     # 1、获取图片验证码
    #     # 准备测试数据
    #     r = random.random()
    #     # 调用API类的方法发送请求，接收响应
    #     response = self.login_api.get_img_code(self.session,r)
    #     logging.info("response = {}".format(response.status_code))
    #     # 对响应数据进行断言
    #     self.assertEqual(200,response.status_code)
    #     # 2、获取短信验证码
    #     # 准备测试数据
    #     # 调用API类的方法发送请求，接收响应
    #     response = self.login_api.get_sms_code(self.session,app.phone1,app.imgCode)
    #     logging.info("response = {}".format(response.json()))
    #     # 对响应数据进行断言
    #     assert_utils(self,response,200,200,"短信发送成功")
    #
    # # 图片验证码错误，获取短信验证码失败
    # def test06_get_sms_code_fail_img_code_error(self):
    #     # 1、获取图片验证码
    #     # 准备测试数据
    #     r = random.random()
    #     # 调用API类的方法发送请求，接收响应
    #     response = self.login_api.get_img_code(self.session,r)
    #     logging.info("response = {}".format(response.status_code))
    #     # 对响应数据进行断言
    #     self.assertEqual(200,response.status_code)
    #     # 2、获取短信验证码
    #     # 准备测试数据
    #     wrong_code = '1234'
    #     # 调用API类的方法发送请求，接收响应
    #     response = self.login_api.get_sms_code(self.session,app.phone1,wrong_code)
    #     logging.info("resposne = {}".format(response.json()))
    #     # 对响应数据进行断言
    #     assert_utils(self,response,200,100,"图片验证码错误")
    #
    # # 图片验证码为空，获取短信验证码失败
    # def test07_get_sms_code_fail_img_code_Null(self):
    #     # 1、获取图片验证码
    #     # 准备测试数据
    #     r = random.random()
    #     # 调用API类的方法发送请求，接收响应
    #     response = self.login_api.get_img_code(self.session,r)
    #     logging.info("response = {}".format(response.status_code))
    #     # 对响应数据进行断言
    #     self.assertEqual(200,response.status_code)
    #     # 2、获取短信验证码
    #     # 准备测试数据
    #     # 调用API类的方法发送请求，接收响应
    #     response = self.login_api.get_sms_code(self.session,app.phone1,'')
    #     logging.info("response = {}".format(response.json()))
    #     # 对响应数据进行断言
    #     assert_utils(self,response,200,100,"图片验证码错误")
    #
    # # 手机号为空，获取短信验证码失败
    # def test08_get_sms_code_fail_phone_is_NULL(self):
    #     # 1、获取图片验证码
    #     # 准备测试数据
    #     r = random.random()
    #     # 调用API类的方法发送请求，接收响应
    #     response = self.login_api.get_img_code(self.session,r)
    #     logging.info("response = {}".format(response.status_code))
    #     # 对响应数据进行断言
    #     self.assertEqual(200,response.status_code)
    #     # 2、获取短信验证码
    #     # 准备测试数据
    #     # 调用API类的方法发送请求，接收响应
    #     response = self.login_api.get_sms_code(self.session,"",app.imgCode)
    #     logging.info("response = {}".format(response.json()))
    #     # 对响应数据进行断言
    #     self.assertEqual(200,response.status_code)
    #     self.assertEqual(100,response.json().get("status"))
    #
    # # 不调用图片验证码接口时，获取短信验证码失败
    # def test09_get_sms_code_fail_no_img_code(self):
    #     # 2、获取短信验证码
    #     # 准备测试数据
    #     # 调用API类的方法发送请求，接收响应
    #     response = self.login_api.get_sms_code(self.session,app.phone1,app.imgCode)
    #     logging.info("response = {}".format(response.json()))
    #     # 对响应数据进行断言
    #     assert_utils(self,response,200,100,"图片验证码错误")
    #
    @parameterized.expand(read_json_data("register.json"))
    def test10_register(self,title,phone,password,verifycode,phone_code,dy_server,invite_phone,status_code,status,description):
        # 1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,phone,app.imgCode)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"短信发送成功")
        # 3、注册
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.register(self.session,phone,password,verifycode,phone_code,dy_server,invite_phone)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,status_code,status,description)