import logging
import unittest
import requests

import app
from api.ApproveAPI import ApproveAPI
from api.LoginAPI import LoginAPI
from utils import assert_utils


class TestApprove(unittest.TestCase):
    realname = "张三"
    card_id = "110117199003070995"
    # 初始化
    def setUp(self) -> None:
        # session初始化
        self.session = requests.Session()
        # API对象初始化
        self.login_api = LoginAPI()
        self.approve_api = ApproveAPI()

    # 清理数据
    def tearDown(self) -> None:
        # 关闭session
        self.session.close()

    # 定义认证测试脚本
    def test01_approve_success(self):
        # 1、用户登录
        respone = self.login_api.login(self.session,app.phone1)
        logging.info("login response = {}".format(respone.json()))
        assert_utils(self,respone,200,200,"登录成功")
        # 2、实名认证
        # 准备测试数据
        # 调用API方法发送请求，接收响应
        respone = self.approve_api.approve(self.session,TestApprove.realname,TestApprove.card_id)
        logging.info("approve response = {}".format(respone.json()))
        # 对响应结果进行断言
        assert_utils(self,respone,200,200,"提交成功!")
        result = TestApprove.card_id[0:3] + "****" + TestApprove.card_id[-3:]
        self.assertEqual(result,respone.json().get("data").get("card_id"))

    # 姓名为空时，实名认证失败
    def test02_approve_fail_realname_is_null(self):
        # 1、用户登录
        response = self.login_api.login(self.session,app.phone2)
        logging.info("login response = {}".format(response.json()))
        assert_utils(self,response,200,200,"登录成功")
        # 2、姓名为空时，实名认证失败
        response = self.approve_api.approve(self.session,"",TestApprove.card_id)
        logging.info("approve response ={}".format(response.json()))
        assert_utils(self,response,200,100,"姓名不能为空")

    # 身份证号为空时，认证失败
    def test03_approve_fail_card_id_is_null(self):
        # 1、用户登录
        response = self.login_api.login(self.session,app.phone2)
        logging.info("login response = {}".format(response.json()))
        assert_utils(self,response,200,200,"登录成功")
        # 2、身份证号为空时，认证失败
        response = self.approve_api.approve(self.session,TestApprove.realname,"")
        logging.info("approve response = {}".format(response.json()))
        assert_utils(self,response,200,100,"身份证号不能为空")

    # 获取认证信息
    def test04_get_approve(self):
        # 1、用户登录
        response = self.login_api.login(self.session,app.phone1)
        logging.info("login response = {}".format(response.json()))
        assert_utils(self,response,200,200,"登录成功")
        # 2、获取认证信息
        response = self.approve_api.get_approve(self.session)
        logging.info("get approve response = {}".format(response.json()))
        self.assertEqual(200,response.status_code)
        result = TestApprove.card_id[0:3] + "****" + TestApprove.card_id[-3:]
        self.assertEqual(result,response.json().get("card_id"))