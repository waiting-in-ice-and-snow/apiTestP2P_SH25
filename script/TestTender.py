import logging
import unittest
import requests

import app
from api.TenderAPI import TenderAPI
from api.LoginAPI import LoginAPI
from utils import assert_utils, third_request_method,DButils


class TestTender(unittest.TestCase):
    loan_id = '862'
    # 初始化
    def setUp(self) -> None:
        # session初始化
        self.session = requests.Session()
        # API对象初始化
        self.login_api = LoginAPI()
        self.tender_api = TenderAPI()

    # 清理数据
    def tearDown(self) -> None:
        # 关闭session
        self.session.close()

    # 获取投资产品详情测试脚本
    def test01_get_loan_info(self):
        # 1、登录
        response = self.login_api.login(self.session,app.phone1)
        logging.info("login resposne = {}".format(response.json()))
        assert_utils(self,response,200,200,"登录成功")
        # 2、获取投资产品详情
        # 准备测试用例的参数数据
        # 调用API方法发送请求，收响应
        response = self.tender_api.get_loan_info(self.session,TestTender.loan_id)
        logging.info("tender response ={}".format(response.json()))
        # 对响应内容进行断言
        assert_utils(self,response,200,200,"OK")
        self.assertEqual(TestTender.loan_id,response.json().get("data").get("loan_info").get("id"))

    # 定义投资的测试脚本
    def test02_trust_tender(self):
        # 1、登录
        response = self.login_api.login(self.session,app.phone1)
        logging.info("login resposne = {}".format(response.json()))
        assert_utils(self,response,200,200,"登录成功")
        # 2、投资
        amount = 100
        response = self.tender_api.trust_tender(self.session,TestTender.loan_id,amount)
        logging.info("tender response = {}".format(response.json()))
        self.assertEqual(200,response.status_code)
        self.assertEqual(200,response.json().get("status"))
        # 3、第三方的投资请求
        # 准备测试数据
        form_data = response.json().get("description").get("form")
        # 调用接口方法发送请求，收响应
        response = third_request_method(form_data)
        logging.info("third tender response = {}".format(response.text))
        # 对响应结果进行断言
        self.assertEqual(200,response.status_code)
        self.assertEqual("InitiativeTender OK",response.text)

    # 定义获取投资列表的测试用例
    def test03_get_mytender_list(self):
        # 1、登录
        response = self.login_api.login(self.session,app.phone1)
        logging.info("login response ={}".format(response.json()))
        assert_utils(self,response,200,200,"登录成功")
        # 2、获取我的投资列表
        status = "tender"
        response = self.tender_api.get_mytenderlist(self.session,status)
        logging.info("get mytenderlist response = {}".format(response.json()))
        self.assertEqual(200,response.status_code)
        # 查询数据库，获取返回的我的投资列表的总数量
        sql = "select count(*) from czbk_finance.fn_tender f INNER JOIN czbk_member.mb_member m ON f.member_id = m.id where m.phone = '{}' and f.status = '-2';"
        result = DButils.execute_sql(sql.format(app.phone1))
        # 检查接口返回的数据是否与数据库中的数据一致
        self.assertEqual(result[0][0],response.json().get("total_items"))