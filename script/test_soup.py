from bs4 import BeautifulSoup

html_data = """
<html>
<head><title>黑马程序员</title></head>
<body>
<p id="test01">软件测试</p>
<p id="test02">2020年</p>
<a href="/api.html">接口测试</a>
<a href="/web.html">Web自动化测试</a>
<a href="/app.html">APP自动化测试</a>
</body>
</html>
"""
# 读取HTML文件
soup = BeautifulSoup(html_data,"html.parser")
# 提取出title的页面元素
ele = soup.title
print(soup.title)
# 提取出title的值
print(ele.get_text())

# 提取出第一个p的元素对象
print(soup.p)
# 提取出第一个P的id属性的值
print(soup.p.get("id"))
# 提取出第一个P的标签的值
print(soup.p.get_text())
# 提取出所有的P的元素对象
print(soup.find_all("p"))

# 将所有a标签中的href属性值和对应的标签值依次打印
for i in soup.find_all("a"):
    print("href={} 对应的标签内容为：{}".format(i.get("href"),i.get_text()))


html_data2 = """
<form name='easypaysubmit' id='easypaysubmit' target='_blank' method='post' action='http://mertest.chinapnr.com/muser/publicRequests'>
	<input name='Version' type='hidden' value='10'/>
	<input name='CmdId' type='hidden' value='UserRegister'/>
	<input name='MerCustId' type='hidden' value='6000060007313892'/>
	<input name='BgRetUrl' type='hidden' value='https://www.baidu.com/'/>
	<input name='RetUrl' type='hidden' value='http://dev-www.zcbk.deayou.com/trust/chinapnr/register/return/20011318124917315444'/>
	<input name='UsrId' type='hidden' value=''/>
	<input name='UsrName' type='hidden' value=''/>
	<input name='IdType' type='hidden' value='00'/>
	<input name='IdNo' type='hidden' value='51343620000113288X'/>
	<input name='UsrMp' type='hidden' value='13210001001'/>
	<input name='UsrEmail' type='hidden' value=''/>
	<input name='MerPriv' type='hidden' value='20011318124917315444'/>
	<input name='ChkValue' type='hidden' value='1784F61D7A6FDB0C900808DEA6DCEA882A138E731234473B84CB2829DF2B66FF032E40697D9668DC4B054A2790BDCF1EF32D2DB4B807CAF7F89829BE7C10520C3AF44DEF8EA2DDD07141C49DDEC147ECEC6A3D8E7E3B751D5308171AB3131668D19822D7F05E2E7CAAC5DB1F5744821B4A8B439E9A4335614B6A2CD8E3467DE5'/>
	<input name='CharSet' type='hidden' value='UTF-8'/>
</form>
<script>document.forms['easypaysubmit'].submit();</script>
"""

soup2 = BeautifulSoup(html_data2,"html.parser")

# 提取第三方请求的url值
url = soup2.form.get("action")
print(url)

# 提取input标签中的name和value的值，作为第三方请求的参数（以键值对的形式保存）
dict_data = {}
for i in soup2.find_all("input"):
    print("参数名：{}，参数值：{}".format(i.get("name"),i.get("value")))
    dict_data[i.get("name")] = i.get("value")
print(dict_data)