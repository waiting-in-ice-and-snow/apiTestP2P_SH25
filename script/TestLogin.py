import logging
import random
import time
import unittest,requests,app
from api.LoginAPI import LoginAPI
from utils import assert_utils,DButils


class TestLogin(unittest.TestCase):
    # 初始化
    @classmethod
    def setUpClass(cls) -> None:
        sql1 = "DELETE f.* from czbk_finance.fn_tender f INNER JOIN czbk_member.mb_member m ON f.member_id = m.id where m.phone in ('{}','{}','{}','{}','{}');"
        sql2 = "DELETE i.* from czbk_member.mb_member_info i INNER JOIN czbk_member.mb_member m ON i.member_id = m.id where m.phone  in ('{}','{}','{}','{}','{}');"
        sql3 = "DELETE l.* from czbk_member.mb_member_login_log l INNER JOIN czbk_member.mb_member m ON l.member_id = m.id where m.phone  in ('{}','{}','{}','{}','{}');"
        sql4 = "DELETE from czbk_member.mb_member_register_log where phone in ('{}','{}','{}','{}','{}');"
        sql5 = "DELETE from czbk_member.mb_member where phone in ('{}','{}','{}','{}','{}');"
        result1 = DButils.execute_sql(sql1.format(app.phone1,app.phone2,app.phone3,app.phone4,app.phone5))
        logging.info("delete sql1：{}, 删除的条数为：{}".format(sql1.format(app.phone1,app.phone2,app.phone3,app.phone4,app.phone5),result1))
        result2 =DButils.execute_sql(sql2.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5))
        logging.info("delete sql2：{}, 删除的条数为：{}".format(sql2.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5),result2))
        result3 =DButils.execute_sql(sql3.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5))
        logging.info("delete sql3：{}, 删除的条数为：{}".format(sql3.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5),result3))
        result4 =DButils.execute_sql(sql4.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5))
        logging.info("delete sql4：{}, 删除的条数为：{}".format(sql4.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5),result4))
        result5 =DButils.execute_sql(sql5.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5))
        logging.info("delete sql5：{}, 删除的条数为：{}".format(sql5.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5),result5))

    # 定义初始化方法
    def setUp(self) -> None:
        # 初始化API对象
        self.login_api = LoginAPI()
        # 初始化session
        self.session = requests.Session()

    # 定义清理方法
    def tearDown(self) -> None:
        # 关闭session
        self.session.close()

    # 随机小数获取图片验证码成功
    def test01_get_img_code_success_random_float(self):
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)

    # 随机整数时获取图片验证码成功
    def test02_get_img_code_success_random_int(self):
        # 准备测试数据
        r = random.randint(100000,999999)
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)

    # 参数为空时，获取图片验证码失败
    def test03_get_img_code_fail_params_is_NULL(self):
        # 准备测试数据
        r = ""
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(404,response.status_code)

    # 参数为字母时，获取图片验证码失败
    def test04_get_img_code_fail_params_is_character(self):
        # 准备测试数据
        r = ''.join(random.sample("abcdefghijklmn",8))
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("resposne = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(400,response.status_code)

    # 参数正确时，获取短信验证码成功
    def test05_get_sms_code_success(self):
        # 1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,app.phone1,app.imgCode)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"短信发送成功")

    # 图片验证码错误，获取短信验证码失败
    def test06_get_sms_code_fail_img_code_error(self):
        # 1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 2、获取短信验证码
        # 准备测试数据
        wrong_code = '1234'
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,app.phone1,wrong_code)
        logging.info("resposne = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,100,"图片验证码错误")

    # 图片验证码为空，获取短信验证码失败
    def test07_get_sms_code_fail_img_code_Null(self):
        # 1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,app.phone1,'')
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,100,"图片验证码错误")

    # 手机号为空，获取短信验证码失败
    def test08_get_sms_code_fail_phone_is_NULL(self):
        # 1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,"",app.imgCode)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        self.assertEqual(100,response.json().get("status"))

    # 不调用图片验证码接口时，获取短信验证码失败
    def test09_get_sms_code_fail_no_img_code(self):
        # 2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,app.phone1,app.imgCode)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,100,"图片验证码错误")

    # 输入必填项，注册成功
    def test10_register_success_params_is_must(self):
        # 1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,app.phone1,app.imgCode)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"短信发送成功")
        # 3、注册
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.register(self.session,app.phone1)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"注册成功")

    # 输入所有参数项，注册成功
    def test11_register_success_paramsis_all(self):
        # 1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,app.phone2,app.imgCode)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"短信发送成功")
        # 3、注册
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.register(self.session,app.phone2,invite_phone=app.phone1)
        logging.info("resposne = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"注册成功")

    # 手机号已存在，注册失败
    def test12_register_fail_phone_is_exist(self):
        # 1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,app.phone1,app.imgCode)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"短信发送成功")
        # 3、注册
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.register(self.session,app.phone1)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,100,"手机已存在!")

    # 密码为空，注册失败
    def test13_register_fail_pwd_is_NULL(self):
        # 1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,app.phone3,app.imgCode)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"短信发送成功")
        # 3、注册
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.register(self.session,app.phone3,"")
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,100,"密码不能为空")

    # 图片验证码错误，注册失败
    def test14_register_fail_img_code_error(self):
        # 1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,app.phone4,app.imgCode)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"短信发送成功")
        # 3、注册
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.register(self.session,app.phone4,imgCode="1234")
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,100,"验证码错误!")

    # 短信验证码错误，注册失败
    def test15_register_fail_sms_code_error(self):
        # 1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,app.phone4,app.imgCode)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"短信发送成功")
        # 3、注册
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.register(self.session,app.phone4,smsCode="123456")
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,100,"验证码错误")

    # 不同意协议，注册失败
    def test16_register_fail_no_agreement(self):
        # 1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,app.phone4,app.imgCode)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"短信发送成功")
        # 3、注册
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.register(self.session,app.phone4,dy_Server="off")
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,100,"请同意我们的条款")

    # 输入正确的用户名密码，登录成功
    def test17_login_success(self):
        # 准备测试数据
        # 调用API方法发送请求，接收响应
        response = self.login_api.login(self.session,app.phone1)
        logging.info("response = {}".format(response.json()))
        # 对响应结果进行断言
        assert_utils(self,response,200,200,"登录成功")

    # 用户不存在时，登录失败
    def test18_login_fail_user_is_not_exist(self):
        # 准备测试数据
        no_user = '13011111111'
        sql = "delete from czbk_member.mb_member where phone = '{}'"
        result = DButils.execute_sql(sql.format(no_user))
        logging.info("删除对应的手机号的用户数据为：{}条".format(result))
        # 调用API方法发送请求，接收响应
        response = self.login_api.login(self.session,no_user)
        logging.info("response = {}".format(response.json()))
        # 对响应结果进行断言
        assert_utils(self,response,200,100,"用户不存在")

    # 密码为空时，登录失败
    def test19_login_fail_pwd_is_NULL(self):
        # 准备测试数据
        # 调用API方法发送请求，接收响应
        response = self.login_api.login(self.session,app.phone1,"")
        logging.info("response ={}".format(response.json()))
        # 对响应结果进行断言
        assert_utils(self,response,200,100,"密码不能为空")

    # 密码错误时，登录失败
    def test20_login_fail_pwd_is_wrong(self):
        wrong_pwd = "error"
        # 1、密码错误1次，登录失败
        response = self.login_api.login(self.session,app.phone1,wrong_pwd)
        logging.info("first error response ={}".format(response.json()))
        assert_utils(self,response,200,100,"密码错误1次,达到3次将锁定账户")
        # 2、密码错误2次，登录失败
        response = self.login_api.login(self.session,app.phone1,wrong_pwd)
        logging.info("second error response ={}".format(response.json()))
        assert_utils(self,response,200,100,"密码错误2次,达到3次将锁定账户")
        # 3、密码错误3次，账号被锁定
        response = self.login_api.login(self.session,app.phone1,wrong_pwd)
        logging.info("third error response ={}".format(response.json()))
        assert_utils(self,response,200,100,"由于连续输入错误密码达到上限，账号已被锁定，请于1.0分钟后重新登录")
        # 4、1分钟内输入正确的账号密码，提示账号被锁定
        response = self.login_api.login(self.session,app.phone1)
        logging.info("response ={}".format(response.json()))
        assert_utils(self,response,200,100,"由于连续输入错误密码达到上限，账号已被锁定，请于1.0分钟后重新登录")
        # 5、1分钟后输入正确的账号密码，提示登录成功
        time.sleep(60)
        response = self.login_api.login(self.session,app.phone1)
        logging.info("response ={}".format(response.json()))
        assert_utils(self,response,200,200,"登录成功")

    # 未登录时判断未登录
    def test21_islogin_no_login(self):
        response = self.login_api.is_login(self.session)
        logging.info("resposne = {}".format(response.json()))
        assert_utils(self,response,200,250,"您未登陆！")

    # 登录时判断已登录
    def test22_islogin(self):
        # 1、先登录
        response = self.login_api.login(self.session,app.phone1)
        logging.info("response ={}".format(response.json()))
        assert_utils(self,response,200,200,"登录成功")
        # 2、判断登录状态
        response = self.login_api.is_login(self.session)
        logging.info("resposne = {}".format(response.json()))
        assert_utils(self,response,200,200,"OK")