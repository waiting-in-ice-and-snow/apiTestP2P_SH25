import logging
import random
import unittest
import requests

import app
from api.LoginAPI import LoginAPI
from api.ApproveAPI import ApproveAPI
from api.TrustAPI import TrustAPI
from api.TenderAPI import TenderAPI
from utils import assert_utils, third_request_method, DButils


class TestWorkflow(unittest.TestCase):
    loan_id = '862'
    # 初始化
    def setUp(self) -> None:
        # session初始化
        self.session = requests.Session()
        # API对象初始化
        self.login_api = LoginAPI()
        self.approve_api = ApproveAPI()
        self.trust_api = TrustAPI()
        self.tender_api = TenderAPI()

    # 数据清理
    def tearDown(self) -> None:
        # 关闭session
        self.session.close()

    @classmethod
    def tearDownClass(cls) -> None:
        sql1 = "DELETE f.* from czbk_finance.fn_tender f INNER JOIN czbk_member.mb_member m ON f.member_id = m.id where m.phone in ('{}','{}','{}','{}','{}');"
        sql2 = "DELETE i.* from czbk_member.mb_member_info i INNER JOIN czbk_member.mb_member m ON i.member_id = m.id where m.phone  in ('{}','{}','{}','{}','{}');"
        sql3 = "DELETE l.* from czbk_member.mb_member_login_log l INNER JOIN czbk_member.mb_member m ON l.member_id = m.id where m.phone  in ('{}','{}','{}','{}','{}');"
        sql4 = "DELETE from czbk_member.mb_member_register_log where phone in ('{}','{}','{}','{}','{}');"
        sql5 = "DELETE from czbk_member.mb_member where phone in ('{}','{}','{}','{}','{}');"
        result1 = DButils.execute_sql(sql1.format(app.phone1,app.phone2,app.phone3,app.phone4,app.phone5))
        logging.info("delete sql1：{}, 删除的条数为：{}".format(sql1.format(app.phone1,app.phone2,app.phone3,app.phone4,app.phone5),result1))
        result2 =DButils.execute_sql(sql2.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5))
        logging.info("delete sql2：{}, 删除的条数为：{}".format(sql2.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5),result2))
        result3 =DButils.execute_sql(sql3.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5))
        logging.info("delete sql3：{}, 删除的条数为：{}".format(sql3.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5),result3))
        result4 =DButils.execute_sql(sql4.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5))
        logging.info("delete sql4：{}, 删除的条数为：{}".format(sql4.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5),result4))
        result5 =DButils.execute_sql(sql5.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5))
        logging.info("delete sql5：{}, 删除的条数为：{}".format(sql5.format(app.phone1, app.phone2, app.phone3, app.phone4, app.phone5),result5))

    # 定义业务流程测试用例
    def test01_tender_flow_001(self):
        # 1、注册
        # 1.1、获取图片验证码
        # 准备测试数据
        r = random.random()
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_img_code(self.session,r)
        logging.info("response = {}".format(response.status_code))
        # 对响应数据进行断言
        self.assertEqual(200,response.status_code)
        # 1.2、获取短信验证码
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.get_sms_code(self.session,app.phone5,app.imgCode)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"短信发送成功")
        # 1.3、注册
        # 准备测试数据
        # 调用API类的方法发送请求，接收响应
        response = self.login_api.register(self.session,app.phone5)
        logging.info("response = {}".format(response.json()))
        # 对响应数据进行断言
        assert_utils(self,response,200,200,"注册成功")
        # 2、登录
        # 准备测试数据
        # 调用API方法发送请求，接收响应
        response = self.login_api.login(self.session,app.phone5)
        logging.info("response = {}".format(response.json()))
        # 对响应结果进行断言
        assert_utils(self,response,200,200,"登录成功")
        # 3、开户
        # 3.2、发送开户请求
        response = self.trust_api.trust_register(self.session)
        logging.info("trust register response = {}".format(response.json()))
        self.assertEqual(200,response.status_code)
        self.assertEqual(200,response.json().get("status"))
        # 3.3、发送第三方开户请求
        # 准备测试数据
        form_data = response.json().get("description").get("form")
        # 调用封装的方法，发送请求接收响应
        response = third_request_method(form_data)
        # 对响应进行断言
        self.assertEqual(200,response.status_code)
        self.assertIn("UserRegister OK",response.text)
        # 4、充值
        # 4.2、获取充值验证码
        r = random.random()
        response = self.trust_api.get_recharge_code(self.session,r)
        logging.info("code response ={}".format(response.status_code))
        self.assertEqual(200,response.status_code)
        # 4.3、充值
        amount = 1000
        response = self.trust_api.trust_recharge(self.session,amount)
        logging.info("recharge response ={}".format(response.json()))
        self.assertEqual(200,response.status_code)
        self.assertEqual(200,response.json().get("status"))
        # 4.4、第三方充值接口
        form_data = response.json().get("description").get("form")
        response = third_request_method(form_data)
        self.assertEqual(200,response.status_code)
        self.assertEqual("NetSave OK",response.text)
        # 5、查询投资产品详情
        # 准备测试用例的参数数据
        # 调用API方法发送请求，收响应
        response = self.tender_api.get_loan_info(self.session,TestWorkflow.loan_id)
        logging.info("tender response ={}".format(response.json()))
        # 对响应内容进行断言
        assert_utils(self,response,200,200,"OK")
        self.assertEqual(TestWorkflow.loan_id,response.json().get("data").get("loan_info").get("id"))
        # 6、投资
        # 6.2、投资
        amount = 100
        response = self.tender_api.trust_tender(self.session,TestWorkflow.loan_id,amount)
        logging.info("tender response = {}".format(response.json()))
        self.assertEqual(200,response.status_code)
        self.assertEqual(200,response.json().get("status"))
        # 6.3、第三方的投资请求
        # 准备测试数据
        form_data = response.json().get("description").get("form")
        # 调用接口方法发送请求，收响应
        response = third_request_method(form_data)
        logging.info("third tender response = {}".format(response.text))
        # 对响应结果进行断言
        self.assertEqual(200,response.status_code)
        self.assertEqual("InitiativeTender OK",response.text)
        # 7、获取我的投资列表
        # 7.2、获取我的投资列表
        status = "tender"
        response = self.tender_api.get_mytenderlist(self.session,status)
        logging.info("get mytenderlist response = {}".format(response.json()))
        self.assertEqual(200,response.status_code)
        # 查询数据库，获取返回的我的投资列表的总数量
        sql = "select count(*) from czbk_finance.fn_tender f INNER JOIN czbk_member.mb_member m ON f.member_id = m.id where m.phone = '{}' and f.status = '-2';"
        result = DButils.execute_sql(sql.format(app.phone5))
        # 检查接口返回的数据是否与数据库中的数据一致
        self.assertEqual(result[0][0],response.json().get("total_items"))