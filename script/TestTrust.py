import logging
import random
import unittest
import requests

import app
from api.TrustAPI import TrustAPI
from api.LoginAPI import LoginAPI
from utils import assert_utils, third_request_method
from bs4 import BeautifulSoup

class TestTrust(unittest.TestCase):
    # 初始化
    def setUp(self) -> None:
        self.session = requests.session()
        self.trust_api = TrustAPI()
        self.login_api = LoginAPI()

    # 清理数据
    def tearDown(self) -> None:
        self.session.close()

    # 测试用例脚本 —— 开户
    def test01_trust_register(self):
        # 1、登录
        response = self.login_api.login(self.session,app.phone1)
        logging.info("login response = {}".format(response.json()))
        assert_utils(self,response,200,200,"登录成功")
        # 2、发送开户请求
        response = self.trust_api.trust_register(self.session)
        logging.info("trust register response = {}".format(response.json()))
        self.assertEqual(200,response.status_code)
        self.assertEqual(200,response.json().get("status"))
        # 3、发送第三方开户请求
        # 准备测试数据
        form_data = response.json().get("description").get("form")
        # 调用封装的方法，发送请求接收响应
        response = third_request_method(form_data)
        # 对响应进行断言
        self.assertEqual(200,response.status_code)
        self.assertIn("UserRegister OK",response.text)

    # 定义充值测试脚本
    def test02_trust_recharge(self):
        # 1、登录
        response = self.login_api.login(self.session,app.phone1)
        logging.info("login response ={}".format(response.json()))
        assert_utils(self,response,200,200,"登录成功")
        # 2、获取充值验证码
        r = random.random()
        response = self.trust_api.get_recharge_code(self.session,r)
        logging.info("code response ={}".format(response.status_code))
        self.assertEqual(200,response.status_code)
        # 3、充值
        amount = 1000
        response = self.trust_api.trust_recharge(self.session,amount)
        logging.info("recharge response ={}".format(response.json()))
        self.assertEqual(200,response.status_code)
        self.assertEqual(200,response.json().get("status"))
        # 4、第三方充值接口
        form_data = response.json().get("description").get("form")
        response = third_request_method(form_data)
        self.assertEqual(200,response.status_code)
        self.assertEqual("NetSave OK",response.text)