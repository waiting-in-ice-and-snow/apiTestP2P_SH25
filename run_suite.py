import unittest
import app,time
from script.TestLogin import TestLogin
from script.TestApprove import TestApprove
from script.TestTrust import TestTrust
from script.TestTender import TestTender
from script.TestWorkflow import TestWorkflow
from htmltestreport import HTMLTestReport

# 1、创建测试套件对象
suite = unittest.TestSuite()

# 2、将所有的测试类，添加到套件对象中
suite.addTest(unittest.makeSuite(TestLogin))
suite.addTest(unittest.makeSuite(TestApprove))
suite.addTest(unittest.makeSuite(TestTrust))
suite.addTest(unittest.makeSuite(TestTender))
suite.addTest(unittest.makeSuite(TestWorkflow))

# 3、运行测试套件，生成测试报告
#report_file = app.BASE_DIR + "/report/report{}.html".format(time.strftime("%Y%m%d_%H%M%S"))
report_file = app.BASE_DIR + "/report/report.html"

runner = HTMLTestReport(report_file,title="p2p金融项目接口自动化",description="v1.0")
runner.run(suite)