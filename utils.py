import json,pymysql
import logging,app
from logging import handlers

import requests
from bs4 import BeautifulSoup


def init_log_config():
    # 1、创建日志器
    logger = logging.getLogger()
    logger.setLevel(logging.INFO)

    # 2、创建控制台处理和文件处理器
    sh = logging.StreamHandler()
    log_path = app.BASE_DIR + "/log/p2p.log"
    fh = logging.handlers.TimedRotatingFileHandler(filename=log_path,when="M",interval=5,backupCount=7,encoding='utf-8')

    # 3、创建格式化器
    f = '%(asctime)s %(levelname)s [%(name)s] [%(filename)s(%(funcName)s:%(lineno)d)] - %(message)s'
    formatter = logging.Formatter(f)

    # 4、将格式化器添加到控制器处理器和文件处理器
    sh.setFormatter(formatter)
    fh.setFormatter(formatter)

    # 5、将处理器添加到日志器
    logger.addHandler(sh)
    logger.addHandler(fh)

# 定义公共的断言方法
def assert_utils(self,response,status_code,status,description):
    self.assertEqual(status_code, response.status_code)
    self.assertEqual(status, response.json().get("status"))
    self.assertEqual(description, response.json().get("description"))

# 定义读取参数化文件的方法
def read_json_data(data_file):
    # 1、打开文件
    file_path = app.BASE_DIR + "/data/" + data_file
    with open(file_path,encoding="utf-8") as f:
        dict_data = json.load(f)
    # 2、读取文件中的数据
    test_cast_list = []
    # 3、遍历写入到新的列表中
    for i in dict_data:
        test_cast_list.append(tuple(i.values()))
    return test_cast_list

# 定义第三方的请求方法
def third_request_method(form_data):
    # 准备测试数据，提取出url和参数
    soup = BeautifulSoup(form_data, "html.parser")
    third_request_url = soup.form.get("action")
    dict_data = {}
    for i in soup.find_all("input"):
        dict_data[i.get("name")] = i.get("value")
    # 发送请求，收响应
    response = requests.post(third_request_url, data=dict_data)
    logging.info("third part response = {}".format(response.text))
    return response

# 定义连接数据的基础库
class DButils():
    conn = None
    cursor = None
    # 建立连接
    @classmethod
    def get_conn(cls):
        if cls.conn is None:
            cls.conn = pymysql.Connect(host="admin-p2p-test.itheima.net",port=3306,user="root",password="Itcast_p2p_20191228",database="czbk_member",charset="utf8",autocommit=True)
        return cls.conn

    # 执行SQL语句并提交
    @classmethod
    def execute_sql(cls,sql):
        result = None
        try:
            cls.conn = cls.get_conn()
            cls.cursor = cls.conn.cursor()
            if sql.split(" ")[0].lower() == "select":
                cls.cursor.execute(sql)
                result = cls.cursor.fetchall()
            else:
                cls.cursor.execute(sql)
                result = cls.cursor.rowcount
        except Exception as e:
            print(e)
            cls.conn.rollback()
            raise e
        finally:
            if cls.cursor:
                cls.cursor.close()
            # 关闭连接
            if cls.conn:
                cls.conn.close()
            cls.cursor = None
            cls.conn = None
            return result