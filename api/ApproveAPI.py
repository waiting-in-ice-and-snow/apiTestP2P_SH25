import app


class ApproveAPI():
    # 初始化方法
    def __init__(self):
        self.approve_url = app.BASE_URL + "/member/realname/approverealname"
        self.get_approve_url = app.BASE_URL + "/member/member/getapprove"

    # 定义认证接口方法
    def approve(self,session,realname,card_id):
        # 接收请求的数据
        approve_data = {"realname":realname, "card_id":card_id}
        # 调用GET/POST方法发送请求，接收响应
        response = session.post(self.approve_url,data=approve_data,files={"x":"y"})
        # 返回响应
        return response

    # 定义获取认证信息接口方法
    def get_approve(self,session):
        return session.post(self.get_approve_url)