import app


class TenderAPI():
    # 初始化
    def __init__(self):
        self.get_loan_info_url = app.BASE_URL + "/common/loan/loaninfo"
        self.trust_tender_url = app.BASE_URL + "/trust/trust/tender"
        self.get_mytenderlist_url = app.BASE_URL + "/loan/tender/mytenderlist"

    # 定义获取产品详情的的API方法
    def get_loan_info(self,session,loan_id):
        # 准备请求数据
        data = {"id":loan_id}
        # 调用session的get/post方法来发送请求，接收响应
        response = session.post(self.get_loan_info_url,data=data)
        # 返回响应内容
        return response

    # 定义投资的API方法
    def trust_tender(self,session,loan_id,amount):
        # 准备请求数据
        tender_data = {"id":loan_id,"amount":amount}
        # 调用session的get/post方法来发送请求，接收响应
        response = session.post(self.trust_tender_url,data=tender_data)
        # 返回响应内容
        return response

    # 定义获取我的投资列表的API方法
    def get_mytenderlist(self,session,status):
        # 准备请求数据
        data = {"status":status}
        # 调用session的get/post方法来发送请求，接收响应
        response = session.post(self.get_mytenderlist_url,data=data)
        # 返回响应内容
        return response