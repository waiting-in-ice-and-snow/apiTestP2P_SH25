import app

class LoginAPI():
    # 创建一个初始化方法
    def __init__(self):
        self.get_img_code_url = app.BASE_URL + "/common/public/verifycode1/{}"
        self.get_sms_code_url = app.BASE_URL + "/member/public/sendSms"
        self.register_url = app.BASE_URL + "/member/public/reg"
        self.login_url = app.BASE_URL + "/member/public/login"
        self.is_login_url = app.BASE_URL + "/member/public/islogin"

    # 定义获取图片验证码的接口方法
    def get_img_code(self,session,r):
        # 接收请求数据
        url = self.get_img_code_url.format(r)
        # 调用GET/POST方法发送请求，接收响应
        response = session.get(url)
        # 返回响应
        return response

    # 定义获取短信验证码的接口方法
    def get_sms_code(self,session,phone,imgCode):
        # 接收请求数据
        sms_data = {'phone':phone,'imgVerifyCode':imgCode,'type':'reg'}
        # 调用GET/POST方法发送请求，接收响应
        response = session.post(self.get_sms_code_url,data=sms_data)
        # 返回响应
        return response

    # 定义注册接口方法
    def register(self,session,phone,pwd="test123",imgCode="8888",smsCode="666666",dy_Server="on",invite_phone=""):
        # 接收请求数据
        register_data = {"phone": phone,
                         "password": pwd,
                         "verifycode": imgCode,
                         "phone_code": smsCode,
                         "dy_server": dy_Server,
                        "invite_phone":invite_phone}
        # 调用GET/POST方法发送请求，接收响应
        response = session.post(self.register_url,data=register_data)
        # 返回响应
        return response

    # 定义登录的API方法
    def login(self,session,phone,pwd="test123"):
        # 接收测试数据
        login_data = {"keywords": phone,"password":pwd}
        # 调用GET/POST方法发送请求，接收响应
        response = session.post(self.login_url,data=login_data)
        # 返回响应
        return response

    # 定义判定登录状态的API方法
    def is_login(self,session):
        return session.post(self.is_login_url)