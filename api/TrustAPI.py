import app

class TrustAPI():
    # 初始化
    def __init__(self):
        self.trust_register_url = app.BASE_URL + "/trust/trust/register"
        self.get_recharge_code_url = app.BASE_URL + "/common/public/verifycode/{}"
        self.trust_regcharge_url = app.BASE_URL + "/trust/trust/recharge"

    # 定义开户请求方法
    def trust_register(self,session):
        return session.post(self.trust_register_url)

    # 定义获取充值验证的方法
    def get_recharge_code(self,session,r):
        url = self.get_recharge_code_url.format(r)
        response = session.get(url)
        return response

    # 定义充值的方法
    def trust_recharge(self,session,amount,valicode="8888"):
        recharge_data = {"paymentType": "chinapnrTrust",
                         "formStr": "reForm",
                         "amount": amount,
                         "valicode": valicode}
        response = session.post(self.trust_regcharge_url,data=recharge_data)
        return response